package com.passlinegame.state;

/**
 * This is the preferred way to declare constants that can be shared.
 *
 * @author Ken Fogel
 * @version 1.0
 */
public final class PassLineConstants {

    /**
     * This class cannot be instantiated because the constructor is private
     */
    private PassLineConstants() {
    }

    public static final int LOSE = 0;
    public static final int WIN = 1;
}
