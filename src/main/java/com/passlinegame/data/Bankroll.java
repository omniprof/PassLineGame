package com.passlinegame.data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * This class represents the current amount of money that you have and the
 * current bet. When the amount of money drops below zero the game will end.
 *
 * Whenever a problem involves currency you should always use BigDecimal
 *
 * @author Ken
 */
public final class Bankroll implements Serializable {

    // BigDecimal is an immutable class. Like a String any change generates a
    // new object.
    private BigDecimal startingBalance;
    private BigDecimal money;
    private BigDecimal bet;

    /**
     * Constructor that initializes all amounts to a BigDecimal value of zero.
     */
    public Bankroll() {
        startingBalance = BigDecimal.ZERO;
        money = BigDecimal.ZERO;
        bet = BigDecimal.ZERO;
    }

    // Its not necessary to comment getters or setters unless they are doing 
    // something other than just setting or getting a value without any other 
    // operations
    
    public BigDecimal getMoney() {
        return money;
    }

    /**
     * If this is the first time you are setting the money then make a copy of 
     * the starting value in startingBalance
     *
     * @param money
     */
    public void setMoney(final BigDecimal money) {
        if (this.money.equals(BigDecimal.ZERO)) {
            startingBalance = new BigDecimal(money.toString());
        }
        this.money = money;
    }

    public BigDecimal getBet() {
        return bet;
    }

    public void setBet(final BigDecimal bet) {
        this.bet = bet;

    }

    /**
     * There is no setter because the value is only initialized when the
     * bankroll is set
     *
     * @return
     */
    public BigDecimal getStartingBalance() {
        return startingBalance;
    }

    @Override
    public String toString() {
        return "Bankroll{" + "money=" + money + ", bet=" + bet + '}';
    }

    // There is no need for equals, hashCode, compareTo or Comparator because 
    // there is only ever one bankroll in this application.
}
