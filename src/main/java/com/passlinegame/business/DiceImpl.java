package com.passlinegame.business;

import java.io.Serializable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This is a hybrid class. It contains the values for both die and it contains a
 * roll method that generates the roll of the dice. A user must roll the dice
 * and then retrieve the values.
 *
 * @author Ken Fogel
 */
public class DiceImpl implements Dice, Serializable {

    private int die1;
    private int die2;
    
    // Real programmers use logging, not System.out.println
    private final static Logger LOG = LoggerFactory.getLogger(DiceImpl.class);

    /**
     * Constructor initializes die so that they are not zero
     */
    public DiceImpl() {
        die1 = 1;
        die2 = 1;
    }

    // Its not necessary to comment getters or setters unless they are doing 
    // something other than just setting or getting a value without any other 
    // operations
    
    @Override
    public int getDie1() {
        return die1;
    }

    @Override
    public int getDie2() {
        return die2;
    }

    /**
     * When only the total is important
     *
     * @return
     */
    @Override
    public int getDiceTotal() {
        return die1 + die2;
    }

    /**
     * Roll the Dice
     */
    @Override
    public void rollTheDice() {
        die1 = (int) (Math.random() * 6.0) + 1;
        die2 = (int) (Math.random() * 6.0) + 1;
    }
    
    @Override
    public String toString() {
        return "Dice{" + "die1=" + die1 + ", die2=" + die2 + '}';
    }

}
