package com.passlinegame.business;

/**
 * Interface class for game logic
 *
 * @author Ken Fogel
 * @version 1.0
 */
public interface PassLine {

    public int passLineFirst();

    public int passLinePoint();
}
