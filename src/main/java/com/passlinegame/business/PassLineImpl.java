package com.passlinegame.business;

import static com.passlinegame.state.PassLineConstants.LOSE;
import static com.passlinegame.state.PassLineConstants.WIN;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Here is the game logic class. It is unaware of the user interface it will
 * work with so it can be used with any interface.
 *
 * @author Ken
 */
public final class PassLineImpl implements PassLine {

    private int gameStatus;
    private final Dice dice;

    // Real programmers use logging, not System.out.println
    private final static Logger LOG = LoggerFactory.getLogger(PassLineImpl.class);
    
    /**
     * Assign a reference to the Dice object to a local reference
     * @param dice 
     */
    public PassLineImpl(Dice dice) {
        this.dice = dice;
    }

    /**
     * The Pass Line game - first throw or come-out roll
     *
     * @return 0=LOSE, 1=WIN, >1=point
     */
    @Override
    public int passLineFirst() {

        // First pass
        switch (dice.getDiceTotal()) {
            case 7:
            case 11: // Win your bet
                gameStatus = WIN;
                break;
            case 2:
            case 3:
            case 12: // Lose your bet
                gameStatus = LOSE;
                break;
            default: // Set the point and keep playing
                gameStatus = dice.getDiceTotal();
                break;
        }
        return gameStatus;
    }

    /**
     * If the point is being played then the game logic is simple. Win with
     * point, lose with 7, otherwise roll again.
     *
     * gameStatus contains the point value. Win or lose sets the gameStatus
     * otherwise it is left alone.
     *
     * @return WIN, LOSE, or current point
     */
    @Override
    public int passLinePoint() {

        if (dice.getDiceTotal() == 7) { // You lose
            gameStatus = LOSE;
        } else if (dice.getDiceTotal() == gameStatus) { // You win
            gameStatus = WIN;
        }
        return gameStatus;
    }
}
