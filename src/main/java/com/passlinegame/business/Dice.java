package com.passlinegame.business;

/**
 * Interface class for Dice
 *
 * @author Ken Fogel
 */
public interface Dice {

    public int getDie1();

    public int getDie2();

    public int getDiceTotal();

    public void rollTheDice();

}
