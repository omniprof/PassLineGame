package com.passlinegame.presentation;

/**
 * Interface class for PassLine Game
 *
 * @author Ken Fogel
 */
public interface PassLineGameInterface {

    /**
     * The game startup and control loop
     */
    public void perform();
}
