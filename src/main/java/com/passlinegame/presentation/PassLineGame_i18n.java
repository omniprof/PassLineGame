package com.passlinegame.presentation;

import com.passlinegame.business.Dice;
import com.passlinegame.business.PassLine;
import com.passlinegame.business.PassLineImpl;
import com.passlinegame.data.Bankroll;
import static com.passlinegame.state.PassLineConstants.LOSE;
import static com.passlinegame.state.PassLineConstants.WIN;
import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.Scanner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This is the preferred user interface or view/controller for the game. This 
 * employs internationalization (i18n) where all text is stored in properties 
 * files called resource bundles. There is one bundle per language supported as 
 * well as a default bundle. You will find the bundles in src/main/resources
 * 
 * Real programmers use i18n!
 *
 * @author Ken
 */
public final class PassLineGame_i18n implements PassLineGameInterface{

    private final Scanner sc;
    private final Bankroll bankroll;
    private final Dice dice;
    private final PassLine passLine;
    private final BigDecimal bankrollLimit;
    
    // If the application allowed the user to change the locale while running 
    // then these will not be final
    private final ResourceBundle msg;
    private final Locale currentLocale;
    
    // Currency must be i18n so values must be formatted
    private final NumberFormat moneyFormat;

    // Real programmers use logging, not System.out.println
    private final static Logger LOG = LoggerFactory.getLogger(PassLineGame_i18n.class);

    /**
     * Accept the bankroll and dice objects. Set the upper limit that is used
     * for setting the bankroll initial value. Instantiate the Scanner and the
     * game logic classes.
     *
     * @param bankroll
     * @param dice
     * @param bankrollLimit
     */
    public PassLineGame_i18n(Bankroll bankroll, Dice dice, BigDecimal bankrollLimit) {
        this.bankroll = bankroll;
        this.dice = dice;
        this.bankrollLimit = bankrollLimit;

        sc = new Scanner(System.in);
        passLine = new PassLineImpl(dice);
        
        // Normally the locale is retrieved from the JVM when the application 
        // begins. In this example so that I can change the language I am setting
        // the locale
        currentLocale = new Locale("en","CA");
        //currentLocale = new Locale("fr", "CA");

        // Alternative naming for locale
        // currentLocale = Locale.CANADA;
        // currentLocale = Locale.CANADA_FRENCH;
        
        // Retrieve the appropriate bundle
        msg = ResourceBundle.getBundle("MessagesBundle", currentLocale);  
        
        // Instantiate a currency formatter
        moneyFormat = NumberFormat.getCurrencyInstance(currentLocale);
        
    }

    /**
     * The game startup and control loop
     */
    @Override
    public void perform() {
        BigDecimal value = requestMoney(msg.getString("for"), bankrollLimit);
        bankroll.setMoney(value);

        boolean playGame = true;

        // Main game loop
        while (playGame) {
            BigDecimal bet = requestMoney(msg.getString("to"), bankroll.getMoney());
            bankroll.setBet(bet);

            rollDice();
            int outcome = passLine.passLineFirst();
            boolean result = determineResult(outcome);

            // Point round loop
            while (result == true) {
                System.out.println(msg.getString("playing") + outcome);
                result = runPoint();
            }
            playGame = showResult();
        }
        System.out.println(msg.getString("starting") + moneyFormat.format(bankroll.getStartingBalance()));
        System.out.println(msg.getString("ending") + moneyFormat.format(bankroll.getMoney()));
    }

    /**
     * Requests a value from the user and if valid returns it as a BigDecimal. 
     * Value must be greater than zero.
     *
     * @param reason String that is appended to the user prompt
     * @param upperLimit value must be greater than zero and less than or equal
     * to this upper limit
     * @return user entered value as a BigDecimal
     */
    private BigDecimal requestMoney(String reason, BigDecimal upperLimit) {
        BigDecimal money = BigDecimal.ZERO;
        while (money.compareTo(BigDecimal.ZERO) <= 0) {
            System.out.println(msg.getString("please") + reason + ": ");
            // Check if there is a String that can be converted to a BigDecimal 
            // waiting in the buffer
            if (sc.hasNextBigDecimal()) {
                // There is so get it
                money = sc.nextBigDecimal();
            }
            // Clean out characters and LF/CR that might be lingering in the buffer
            sc.nextLine();

            // Will be zero if a value was not assigned because it was invalid
            if (money.compareTo(BigDecimal.ZERO) <= 0) {
                System.out.println(msg.getString("invalid"));
                // The upperLimit may apply to the bankroll or an individual bet
            } else if (money.compareTo(upperLimit) > 0) {
                money = BigDecimal.ZERO;
                System.out.println(msg.getString("exceeds") + moneyFormat.format(upperLimit));
            }
        }
        return money;
    }

    /**
     * Prompt the user to roll the dice and display the dice values.
     */
    private void rollDice() {
        System.out.println(msg.getString("press"));
        sc.nextLine();
        dice.rollTheDice();
        System.out.println(msg.getString("roll") + dice.getDie1() + " + " + dice.getDie2());
    }

    /**
     * Based on the outcome of the game either add (WIN) or subtract (LOSE) the
     * bet or keep playing if its a point round of the game.
     *
     * @param outcome WIN, LOSE, or point value
     * @return result as a boolean where true means we are in a point round and
     * false means the game is finished.
     */
    private boolean determineResult(int outcome) {
        boolean result = false;

        switch (outcome) {
            case WIN:
                bankroll.setMoney(bankroll.getMoney().add(bankroll.getBet()));
                System.out.println(msg.getString("win"));
                break;
            case LOSE:
                bankroll.setMoney(bankroll.getMoney().subtract(bankroll.getBet()));
                System.out.println(msg.getString("lose"));
                break;
            default:
                result = true;
                break;
        }
        return result;
    }

    /**
     * Show the current balance. Determine if the balance is at zero in which
     * case the game ends Otherwise as the user if they wish to play again.
     *
     * @return playGame a boolean that indicates end of game (false) or not
     * change the current state of playGame that should be true.
     */
    private boolean showResult() {
        boolean playGame = true;
        // Display the balance
        System.out.println(msg.getString("balance") + moneyFormat.format(bankroll.getMoney()));
        if (bankroll.getMoney().compareTo(BigDecimal.ZERO) > 0) {
            // There is money left to gamble with so ask if it should play again
            System.out.println(msg.getString("again"));
            String again = sc.nextLine();
            if (!again.equals(msg.getString("yes"))) {
                playGame = false;
            }
        } else {
            System.out.println(msg.getString("sorry"));
            playGame = false;
        }
        return playGame;
    }

    /**
     * Play the point round
     *
     * @param pointRoll
     * @return
     */
    private boolean runPoint() {
        rollDice();
        int pointPlay = passLine.passLinePoint();
        boolean result = determineResult(pointPlay);
        return result;

        // could also be coded:
        // rollDice();
        // return(determineResult(passLine.passLinePoint());
    }
}
