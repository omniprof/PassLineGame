package com.passlinegame.presentation;

import com.passlinegame.business.Dice;
import com.passlinegame.business.PassLine;
import com.passlinegame.business.PassLineImpl;
import com.passlinegame.data.Bankroll;
import static com.passlinegame.state.PassLineConstants.LOSE;
import static com.passlinegame.state.PassLineConstants.WIN;
import java.math.BigDecimal;
import java.util.Scanner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This is the user interface or view/controller for the game
 * All messages are hard coded into the source file, never a good idea.
 *
 * @author Ken
 */
public final class PassLineGame implements PassLineGameInterface {

    private final Scanner sc;
    private final Bankroll bankroll;
    private final Dice dice;
    private final PassLine passLine;
    private final BigDecimal bankrollLimit;

    // Real programmers use logging, not System.out.println
    private final static Logger LOG = LoggerFactory.getLogger(PassLineGame.class);

    /**
     * Accept the bankroll and dice objects. Set the upper limit that is used
     * for setting the bankroll initial value. Instantiate the Scanner and the
     * game logic classes.
     *
     * @param bankroll
     * @param dice
     * @param bankrollLimit
     */
    public PassLineGame(Bankroll bankroll, Dice dice, BigDecimal bankrollLimit) {
        this.bankroll = bankroll;
        this.dice = dice;
        this.bankrollLimit = bankrollLimit;

        sc = new Scanner(System.in);
        passLine = new PassLineImpl(dice);
    }

    /**
     * The game startup and control loop
     */
    @Override
    public void perform() {
        BigDecimal value = requestMoney("for the Bankroll", bankrollLimit);
        bankroll.setMoney(value);

        boolean playGame = true;

        // Main game loop
        while (playGame) {
            BigDecimal bet = requestMoney("to bet", bankroll.getMoney());
            bankroll.setBet(bet);

            rollDice();
            int outcome = passLine.passLineFirst();
            boolean result = determineResult(outcome);

            // Point round loop
            while (result == true) {
                System.out.println("Playing for point " + outcome);
                result = runPoint();
            }
            playGame = showResult();
        }
        System.out.println("Starting balance: $" + bankroll.getStartingBalance());
        System.out.println("Ending balance: $" + bankroll.getMoney());
    }

    /**
     * Requests a value from the user and if valid returns it as a BigDecimal.
     * Value must be greater than zero.
     *
     * @param reason String that is appended to the user prompt
     * @param upperLimit value must be greater than zero and less than or equal
     * to this upper limit
     * @return user entered value as a BigDecimal
     */
    private BigDecimal requestMoney(String reason, BigDecimal upperLimit) {
        BigDecimal money = BigDecimal.ZERO;
        while (money.compareTo(BigDecimal.ZERO) <= 0) {
            System.out.println("Please enter the amount of money " + reason + ": ");
            // Check if there is a String that can be converted to a BigDecimal 
            // waiting in the buffer
            if (sc.hasNextBigDecimal()) {
                // There is so get it
                money = sc.nextBigDecimal();
            }
            // Clean out characters and LF/CR that might be lingering in the buffer
            sc.nextLine();

            // Will be zero if a value was not assigned because it was invalid
            if (money.compareTo(BigDecimal.ZERO) <= 0) {
                System.out.println("I'm sorry, your input was invalid.");
                // The upperLimit may apply to the bankroll or an individual bet
            } else if (money.compareTo(upperLimit) > 0) {
                money = BigDecimal.ZERO;
                System.out.println("I'm sorry, your input exceeds your limit or the size of your bankroll of $" + upperLimit);
            }
        }
        return money;
    }

    /**
     * Prompt the user to roll the dice and display the dice values.
     */
    private void rollDice() {
        System.out.println("Press enter to roll the dice");
        sc.nextLine();
        dice.rollTheDice();
        System.out.println("Roll: " + dice.getDie1() + " + " + dice.getDie2());
    }

    /**
     * Based on the outcome of the game either add (WIN) or subtract (LOSE) the
     * bet or keep playing if its a point round of the game.
     *
     * @param outcome WIN, LOSE, or point value
     * @return result as a boolean where true means we are in a point round and
     * false means the game is finished.
     */
    private boolean determineResult(int outcome) {
        boolean result = false;

        switch (outcome) {
            case WIN:
                bankroll.setMoney(bankroll.getMoney().add(bankroll.getBet()));
                System.out.println("You Win!");
                break;
            case LOSE:
                bankroll.setMoney(bankroll.getMoney().subtract(bankroll.getBet()));
                System.out.println("You Lose!");
                break;
            default:
                result = true;
                break;
        }
        return result;
    }

    /**
     * Show the current balance. Determine if the balance is at zero in which
     * case the game ends Otherwise as the user if they wish to play again.
     *
     * @return playGame a boolean that indicates end of game (false) or not
     * change the current state of playGame that should be true.
     */
    private boolean showResult() {
        boolean playGame = true;
        // Display the balance
        System.out.println("Current balance: $" + bankroll.getMoney());
        if (bankroll.getMoney().compareTo(BigDecimal.ZERO) > 0) {
            // There is money left to gamble with so ask if it should play again
            System.out.println("Play again? : y=yes");
            String again = sc.nextLine();
            if (!again.equals("y")) {
                playGame = false;
            }
        } else {
            System.out.println("Sorry, you have run out of money.");
            playGame = false;
        }
        return playGame;
    }

    /**
     * Play the point round
     *
     * @param pointRoll
     * @return
     */
    private boolean runPoint() {
        rollDice();
        int pointPlay = passLine.passLinePoint();
        boolean result = determineResult(pointPlay);
        return result;

        // could also be coded:
        // rollDice();
        // return(determineResult(passLine.passLinePoint());
    }
}
