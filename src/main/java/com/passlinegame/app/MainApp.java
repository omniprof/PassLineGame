package com.passlinegame.app;

import com.passlinegame.business.Dice;
import com.passlinegame.business.DiceImpl;
import com.passlinegame.data.Bankroll;
import com.passlinegame.presentation.PassLineGame;
import com.passlinegame.presentation.PassLineGameInterface;
import com.passlinegame.presentation.PassLineGame_i18n;
import java.math.BigDecimal;

/**
 * This application plays the dice game called PassLine. The objects for the
 * users bankroll and for the game dice are created here. When this game is
 * expanded to additional games then the bankroll and dice will be shared by all
 * the games.
 *
 * @author Ken Fogel
 * @version 0.01
 */
public class MainApp {

    /*
       Object references initialized in a constructor that are not intended to be
       reallocated (use new to create a new instance, sending the previous
       instance to garbage collection) should be marked as final. This will
       result in an error message in the IDE or when compiled at the command
       line if the final reference is rellocated.
     */
    private final Bankroll bankroll;
    private final Dice dice;
    private final PassLineGameInterface passLineGame;

    /**
     * Constructor that instantiates the class variables. If we added other
     * games such as Any 7 or Field Bet then they will have the same bankroll
     * and dice.
     */
    public MainApp() {
        bankroll = new Bankroll();
        dice = new DiceImpl();
        passLineGame = new PassLineGame(bankroll, dice, BigDecimal.valueOf(1000));
//        passLineGame = new PassLineGame_i18n(bankroll, dice, BigDecimal.valueOf(1000));
    }

    /**
     * This is the first domino. Here is where the program flow begins and ends.
     */
    public void perform() {
        passLineGame.perform();
    }

    /**
     * This is where it all begins. Should never be more than 3 lines long.
     *
     * @param args
     */
    public static void main(String[] args) {
        MainApp ma = new MainApp();
        ma.perform();
        System.exit(0);
    }

}
