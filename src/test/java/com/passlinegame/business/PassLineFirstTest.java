package com.passlinegame.business;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.mockito.Mockito;
import static org.mockito.Mockito.when;

import static com.passlinegame.state.PassLineConstants.LOSE;
import static com.passlinegame.state.PassLineConstants.WIN;
import java.util.Arrays;
import java.util.Collection;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

/**
 * This test examines the results when on the first rollin the game.
 *
 * @author Ken Fogel
 * @version 1.0
 */
@RunWith(Parameterized.class)
public class PassLineFirstTest {

    /**
     * The data in each row is used in each test. I leave it to the reader to
     * come up with a better way to generate the test data and results. Luckily
     * we only need 11 values.
     *
     * @return
     */
    @Parameterized.Parameters(name = "{index} role={0} result={1}")
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
            // Lose values
            {2, LOSE},
            {3, LOSE},
            {12, LOSE},
            // Win values
            {7, WIN},
            {11, WIN},
            // Point values
            {4, 4},
            {5, 5},
            {6, 6},
            {8, 8},
            {9, 9},
            {10, 10}
        });
    }

    private Dice dice;
    private PassLine passLine;

    private final int diceRoll;
    private final int result;

    /**
     * Every time the JUnit framework instantiates this object it delivers a row
     * from the static data method.
     *
     * @param diceRoll
     * @param result
     */
    public PassLineFirstTest(int diceRoll, int result) {
        this.diceRoll = diceRoll;
        this.result = result;
    }

    /**
     * Create a mock of the dice class so that we can control the value from the
     * dice and instantiate the PassLineImpl object with the mock DiceImpl.
     *
     */
    @Before
    public void setUp() {
        dice = Mockito.mock(DiceImpl.class);
        passLine = new PassLineImpl(dice);
    }

    /**
     * Test that on the first roll that the lose, win and continue rolling
     * (point) states are as expected.
     *
     */
    @Test
    public void testResult() {
        when(dice.getDiceTotal()).thenReturn(diceRoll);
        assertEquals("Result with " + diceRoll + ": ", passLine.passLineFirst(), result);
    }
}
