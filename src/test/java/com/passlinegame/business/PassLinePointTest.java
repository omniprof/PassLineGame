package com.passlinegame.business;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.mockito.Mockito;
import static org.mockito.Mockito.when;

import static com.passlinegame.state.PassLineConstants.LOSE;
import static com.passlinegame.state.PassLineConstants.WIN;
import java.util.Arrays;
import java.util.Collection;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

/**
 * This test examines the results when a point is achieved in the game.
 *
 * @author Ken Fogel
 * @version 1.0
 */
@RunWith(Parameterized.class)
public class PassLinePointTest {

    /**
     * The data in each row is used in each test. I leave it to the reader to
     * come up with a better way to generate the test data and results. Luckily
     * we are working with just two 6 sided die.
     *
     * @return
     */
    @Parameterized.Parameters(name = "{index} point={0} diceRoll={1} result={2}")
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
            // Regardless of point should always lose
            {4, 7, LOSE},
            {5, 7, LOSE},
            {6, 7, LOSE},
            {8, 7, LOSE},
            {9, 7, LOSE},
            {10, 7, LOSE},
            // Only win when point and diceRoll are equals otherwise return point
            {4, 4, WIN},
            {4, 5, 4},
            {4, 6, 4},
            {4, 8, 4},
            {4, 9, 4},
            {4, 10, 4},
            {5, 4, 5},
            {5, 5, WIN},
            {5, 6, 5},
            {5, 8, 5},
            {5, 9, 5},
            {5, 10, 5},
            {6, 4, 6},
            {6, 5, 6},
            {6, 6, WIN},
            {6, 8, 6},
            {6, 9, 6},
            {6, 10, 6},
            {8, 4, 8},
            {8, 5, 8},
            {8, 6, 8},
            {8, 8, WIN},
            {8, 9, 8},
            {8, 10, 8},
            {9, 4, 9},
            {9, 5, 9},
            {9, 6, 9},
            {9, 8, 9},
            {9, 9, WIN},
            {9, 10, 9},
            {10, 4, 10},
            {10, 5, 10},
            {10, 6, 10},
            {10, 8, 10},
            {10, 9, 10},
            {10, 10, WIN}
        });
    }

    private Dice dice;
    private PassLine passLine;

    private final int point;
    private final int diceRoll;
    private final int result;

    /**
     * Every time the JUnit framework instantiates this object it delivers a row
     * from the static data method.
     *
     * @param point
     * @param diceRoll
     * @param result
     */
    public PassLinePointTest(int point, int diceRoll, int result) {
        this.point = point;
        this.diceRoll = diceRoll;
        this.result = result;
    }

    /**
     * Create a mock of the dice class so that we can control the value from the
     * dice. Play the first throw forcing a point situation by using the point
     * value as the dice throw. Switch to the diceRoll value for the dice throw
     * subsequently.
     */
    @Before
    public void setUp() {
        dice = Mockito.mock(DiceImpl.class);
        passLine = new PassLineImpl(dice);
        when(dice.getDiceTotal()).thenReturn(point);
        passLine.passLineFirst();
    }

    /**
     * Test that when rolling on the point that the lose, win and continue
     * rolling states are as expected.
     */
    @Test
    public void testResult() {
        when(dice.getDiceTotal()).thenReturn(diceRoll);
        String msg = "Point = " + point + " roll = " + diceRoll + " result = " + result;
        assertEquals("Result with " + diceRoll + ": ", passLine.passLinePoint(), result);
    }
}
